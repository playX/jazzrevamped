#![allow(non_camel_case_types)]
extern crate rvm;

use rvm::runtime::instruction::Instruction;
use rvm::runtime::object::*;
use rvm::runtime::value::Type;

fn main() {
    let mut class = Class::new("Test".into(), EClass::ClassRef("Test".into()), None);

    let code = vec![Instruction::br(7),
                    Instruction::nop,
                    Instruction::ldloc("i".into()),
                    Instruction::ldci(1),
                    Instruction::add,
                    Instruction::stloc("i".into()),
                    Instruction::br(9),
                    Instruction::nop,
                    Instruction::ldci(0),
                    Instruction::stloc("i".into()),
                    Instruction::ldloc("i".into()),
                    Instruction::ldci(10000),
                    Instruction::cgt,
                    Instruction::brtrue(1),
                    Instruction::ldloc("i".into()),
                    Instruction::ret,];

    let method = Method::new("main".into(),
                             vec![],
                             Type::Int,
                             ExecuteVariant::Internal(code));
    class.methods.insert("main".into(), method);

    let method = class.find_method("main").expect("not found");

    let ret = method.invoke(&class, vec![]);
    println!("{:?}", ret);
}
