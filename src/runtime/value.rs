use super::byte;
use super::object::Class;
use super::object::EClass;
use std::cell::RefCell;
use std::num::Wrapping;
use std::rc::Rc;

#[derive(Clone, Debug)]
pub enum Value {
    Int(Wrapping<i32>),
    Long(Wrapping<i64>),
    ClassRef(RefCell<Class>),
    Float(f32),
    Double(f64),
    Bool(bool),
    Array(RefCell<Array>),
    Null,
}

#[derive(Clone, Debug)]
pub struct Array {
    pub ty: Type,
    class: Rc<Class>,
    array: Vec<Value>,
}
impl Array {
    pub fn new(class: Rc<Class>, length: i32) -> Self {
        if length < 0
        {
            panic!("[ERROR] Negative array size");
        }

        match class.sig
        {
            EClass::ClassRef(_) => panic!("[ERROR] can't construct array from scalar class"),
            EClass::Array(ref ty) =>
            {
                let mut array = Vec::with_capacity(length as usize);
                for _ in 0..length
                {
                    array.push(ty.default_value());
                }
                Array { ty: *ty.clone(),
                        class: class.clone(),
                        array }
            },
        }
    }

    pub fn get(&self, idx: usize) -> Value {
        self.array[idx].clone()
    }

    pub fn set(&mut self, idx: usize, v: Value) {
        self.array[idx] = v;
    }

    pub fn get_class(&self) -> Rc<Class> {
        self.class.clone()
    }

    pub fn size(&self) -> i32 {
        self.array.len() as i32
    }
}

pub struct NativeMethod(pub &'static Fn(Vec<Value>) -> Option<Value>);

use std::fmt;
impl fmt::Debug for NativeMethod {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "<native method>")
    }
}

impl Clone for NativeMethod {
    fn clone(&self) -> NativeMethod {
        NativeMethod(self.0.clone())
    }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
pub enum Type {
    Int,
    Long,
    Double,
    Void,
    Bool,
    Float,
    Class(EClass),
}

impl Type {
    pub fn default_value(&self) -> Value {
        use self::Type::*;
        match *self
        {
            Int => Value::Int(Wrapping(0)),
            Long => Value::Long(Wrapping(0)),
            Float => Value::Float(0.0),
            Double => Value::Double(0.0),
            Void => Value::Null,
            Class(_) => Value::Null,
            Bool => Value::Bool(false),
        }
    }
    pub fn check_type(&self, v: Value) -> Result<(), String> {
        match (self, v)
        {
            (Type::Int, Value::Int(_)) => Ok(()),
            (Type::Double, Value::Double(_)) => Ok(()),
            (Type::Void, Value::Null) => Ok(()),
            (Type::Class(ref ec), Value::ClassRef(ref class)) => match (ec, &class.borrow().sig)
            {
                (EClass::Array(ref typ), EClass::Array(ref etyp)) =>
                {
                    let typ = *typ.clone();
                    let etyp = *etyp.clone();
                    if typ == etyp
                    {
                        return Ok(());
                    }
                    else
                    {
                        return Err(format!("Expected `{:?}`,found `{:?}`", typ, etyp));
                    }
                },
                (EClass::ClassRef(ref cname), EClass::ClassRef(ref name)) =>
                {
                    if cname == name
                    {
                        return Ok(());
                    }
                    else
                    {
                        return Err(format!("Expected `{}`,found `{}`", cname, name));
                    }
                },
                _ => unimplemented!(),
            },
            (Type::Bool, Value::Bool(_)) => Ok(()),
            (Type::Long, Value::Long(_)) => Ok(()),
            (Type::Float, Value::Float(_)) => Ok(()),

            (v, v2) => return Err(format!("Expected `{:?}`, found `{:?}`", v, v2)),
        }
    }
}
