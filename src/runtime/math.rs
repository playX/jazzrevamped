use super::value::Value;

use self::Value::*;

pub fn add(v1: Value, v2: Value) -> Value {
    match (v1, v2)
    {
        (Float(f1), Float(f2)) => Float(f1 + f2),
        (Double(d), Double(d2)) => Double(d + d2),
        (Int(i), Int(i2)) => Int(i + i2),
        (Long(l), Long(l2)) => Long(l + l2),
        (v, v2) => panic!("Cannot add `{:?}` to `{:?}`", v, v2),
    }
}

pub fn sub(v1: Value, v2: Value) -> Value {
    match (v1, v2)
    {
        (Float(f1), Float(f2)) => Float(f1 - f2),
        (Double(d), Double(d2)) => Double(d - d2),
        (Int(i), Int(i2)) => Int(i - i2),
        (Long(l), Long(l2)) => Long(l - l2),
        (v, v2) => panic!("Cannot sub `{:?}` to `{:?}`", v, v2),
    }
}

pub fn ge(v1: Value, v2: Value) -> Value {
    match (v1, v2)
    {
        (Float(f1), Float(f2)) => Bool(f1 >= f2),
        (Double(d), Double(d2)) => Bool(d >= d2),
        (Int(i), Int(i2)) => Bool(i >= i2),
        (Long(l), Long(l2)) => Bool(l >= l2),
        (v, v2) => panic!("Cannot ge `{:?}` to `{:?}`", v, v2),
    }
}

pub fn gt(v1: Value, v2: Value) -> Value {
    match (v1, v2)
    {
        (Float(f1), Float(f2)) => Bool(f1 > f2),
        (Double(d), Double(d2)) => Bool(d > d2),
        (Int(i), Int(i2)) => Bool(i > i2),
        (Long(l), Long(l2)) => Bool(l > l2),
        (v, v2) => panic!("Cannot gt `{:?}` to `{:?}`", v, v2),
    }
}

pub fn lt(v1: Value, v2: Value) -> Value {
    match (v1, v2)
    {
        (Float(f1), Float(f2)) => Bool(f1 < f2),
        (Double(d), Double(d2)) => Bool(d < d2),
        (Int(i), Int(i2)) => Bool(i < i2),
        (Long(l), Long(l2)) => Bool(l < l2),
        (v, v2) => panic!("Cannot lt `{:?}` to `{:?}`", v, v2),
    }
}

pub fn le(v1: Value, v2: Value) -> Value {
    match (v1, v2)
    {
        (Float(f1), Float(f2)) => Bool(f1 <= f2),
        (Double(d), Double(d2)) => Bool(d <= d2),
        (Int(i), Int(i2)) => Bool(i <= i2),
        (Long(l), Long(l2)) => Bool(l <= l2),
        (v, v2) => panic!("Cannot le `{:?}` to `{:?}`", v, v2),
    }
}
