use super::callframe::CallFrame;
use super::value::Type;
use super::value::Value;
use std::cell::RefCell;
use std::collections::HashMap;
use std::rc::Rc;
#[derive(Clone, Debug)]
pub struct Class {
    pub name: String,
    pub methods: HashMap<String, Method>,
    pub fields: HashMap<String, Field>,
    pub superclass: Option<Rc<Class>>,
    pub sig: EClass,
}

#[derive(Debug, Clone)]
pub enum EClass {
    ClassRef(String),
    Array(Box<Type>),
}

use std::cmp::{Eq, Ord, Ordering, PartialEq};

impl Eq for EClass {}

impl PartialOrd for EClass {
    fn partial_cmp(&self, other: &EClass) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for EClass {
    fn eq(&self, other: &EClass) -> bool {
        match (self, other)
        {
            (EClass::ClassRef(name), EClass::ClassRef(cname)) => name == cname,
            (EClass::Array(typ), EClass::Array(typ2)) =>
            {
                let typ = *typ.clone();
                let typ2 = *typ2.clone();
                typ == typ2
            },
            _ => panic!("Cannot compare"),
        }
    }
}

impl Ord for EClass {
    fn cmp(&self, other: &EClass) -> Ordering {
        match (self, other)
        {
            (EClass::ClassRef(name), EClass::ClassRef(cname)) => name.cmp(cname),
            (EClass::Array(typ), EClass::Array(typ2)) =>
            {
                let typ = *typ.clone();
                let typ2 = *typ2.clone();
                typ.cmp(&typ2)
            },
            _ => panic!("Cannot compare"),
        }
    }
}

impl Class {
    pub fn new(name: String, sig: EClass, sc: Option<Rc<Class>>) -> Class {
        Class { name,
                methods: HashMap::new(),
                fields: HashMap::new(),
                superclass: sc,
                sig: sig }
    }

    pub fn find_method(&self, s: &str) -> Option<&Method> {
        self.methods
            .get(s)
            .or_else(|| self.superclass.as_ref().and_then(|sc| sc.find_method(s)))
    }

    pub fn find_field(&self, s: &str) -> Option<&Field> {
        self.fields
            .get(s)
            .or_else(|| self.superclass.as_ref().and_then(|sc| sc.find_field(s)))
    }
}

#[derive(Clone, Debug)]
pub struct Field {
    pub name: String,
    pub typ: Type,
    pub val: Value,
}
#[derive(Clone, Debug)]
pub struct Method {
    pub args: Vec<(String, Type)>,
    pub name: String,
    pub ret_ty: Type,
    pub method: ExecuteVariant,
}

impl Method {
    pub fn new(name: String, args: Vec<(String, Type)>, ret: Type, m: ExecuteVariant) -> Method {
        Method { name,
                 args,
                 ret_ty: ret,
                 method: m }
    }

    pub fn invoke(&self, class: &Class, args: Vec<Value>) -> Value {
        let result = match self.method
        {
            ExecuteVariant::Internal(ref code) =>
            {
                let mut locals = HashMap::new();

                for value in args.iter()
                {
                    for (n, t) in self.args.iter()
                    {
                        if t.check_type(value.clone()).is_err()
                        {
                            panic!("Bad types");
                        }
                        else
                        {
                            locals.insert(n.to_string(), value.clone());
                        }
                    }
                }

                let mut frame = CallFrame::new(class);
                frame.code = code.clone();
                frame.locals = locals;
                return frame.run();
            },
            ExecuteVariant::Native(ref method) => unimplemented!(),
        };
    }
}

use super::instruction::Instruction;
#[derive(Clone, Debug)]
pub enum ExecuteVariant {
    Internal(Vec<Instruction>),
    Native(super::value::NativeMethod),
}
