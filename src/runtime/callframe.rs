use super::instruction::Instruction;
use super::math;
use super::object::Class;
use super::object::EClass;
use super::value::Value;
use std::cell::RefCell;
use std::collections::HashMap;
use std::num::Wrapping;
#[derive(Clone, Debug)]
pub struct CallFrame<'a> {
    pub current_class: &'a Class,
    pub stack: Vec<Value>,
    pub code: Vec<Instruction>,
    pub pc: usize,
    pub locals: HashMap<String, Value>,
}

impl<'a> CallFrame<'a> {
    pub fn new(class: &'a Class) -> CallFrame<'a> {
        CallFrame { current_class: class,
                    stack: Vec::new(),
                    code: Vec::new(),
                    pc: 0,
                    locals: HashMap::new() }
    }

    pub fn run(&mut self) -> Value {
        macro_rules! push {
            ($v: expr,$s: expr) => {
                $v.stack.push($s);
            };
        }

        macro_rules! pop {
            ($s:expr,$v:path) => {{
                let v = $s.stack.pop();
                if v.is_none()
                {
                    panic!("Stack empty");
                }
                match v.unwrap()
                {
                    $v(v) => v,
                    v => panic!("[ERROR] Expected value with `{}` type,found `{:?}`",
                                stringify!($v),
                                v),
                }
            }};
            ($s:expr) => {
                $s.stack.pop().expect("stack empty")
            };
        }
        let mut rets = false;
        let mut retv = Value::Null;

        while self.pc < self.code.len()
        {
            if rets
            {
                break;
            }

            let op = self.code[self.pc].clone();
            use self::Instruction::*;
            match op
            {
                ldcc(class) =>
                {
                    push!(self, Value::ClassRef(RefCell::new(class)));
                },
                ldcd(double) =>
                {
                    push!(self, Value::Double(double));
                },

                ldcf(float) =>
                {
                    push!(self, Value::Float(float));
                },
                ldci(int) =>
                {
                    push!(self, Value::Int(Wrapping(int)));
                },

                ldcl(long) =>
                {
                    push!(self, Value::Long(Wrapping(long)));
                },

                ldloc(ref lname) =>
                {
                    push!(self,
                          self.locals.get(lname).expect("Local not found").clone());
                },

                stloc(ref lname) =>
                {
                    self.locals.insert(lname.to_string(), pop!(self));
                },
                nop =>
                {},

                ldnull =>
                {
                    push!(self, Value::Null);
                },

                brtrue(idx) =>
                {
                    let b = pop!(self, Value::Bool);
                    if b
                    {
                        self.pc = idx;
                    }
                },
                br(idx) =>
                {
                    self.pc = idx;
                },

                aset(idx) =>
                {
                    let array = self.stack.pop();
                    match array.unwrap()
                    {
                        Value::Array(class) =>
                        {
                            let val = pop!(self);
                            class.borrow().ty.check_type(val.clone());
                            class.borrow_mut().set(idx, val);
                        },
                        _ => unimplemented!(),
                    }
                },

                aget(idx) =>
                {
                    let array = pop!(self, Value::Array);
                    push!(self, array.borrow().get(idx));
                },

                add =>
                {
                    let (v, v2) = (pop!(self), pop!(self));
                    push!(self, math::add(v, v2))
                },

                sub =>
                {
                    let (v, v2) = (pop!(self), pop!(self));
                    push!(self, math::sub(v, v2));
                },

                cge =>
                {
                    let (v, v2) = (pop!(self), pop!(self));
                    push!(self, math::ge(v, v2));
                },
                cgt =>
                {
                    let (v, v2) = (pop!(self), pop!(self));
                    push!(self, math::gt(v, v2));
                },

                ret =>
                {
                    rets = true;
                    retv = pop!(self);
                },

                _ => unimplemented!(),
            }
            self.pc += 1;
        }
        return retv;
    }
}
