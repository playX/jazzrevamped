pub mod callframe;
pub mod instruction;
pub mod math;
pub mod object;
pub mod value;

pub type byte = u8;
