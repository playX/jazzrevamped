#![allow(non_camel_case_types)]

extern crate serde;
#[macro_use]
extern crate serde_derive;

pub mod runtime;

#[macro_export]
macro_rules! encode {
    ($v: expr; $t: tt) => {
        use std::mem;
        unsafe {
            mem::transmute::<$t,[byte;mem::size_of::<$t>()]($v)
        }
    };
}

#[macro_export]
macro_rules! decode {
    ($v: expr; $t: tt) => {
        use std::mem;
        unsafe { mem::transmute::<[byte; mem::size_of::<$t>()], $t>($v) }
    };
}
